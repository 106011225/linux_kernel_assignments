# Linux kernel assignment: Character devices


## Goal

Implement a kernel module that can store 100 Bytes data.  

Use these commands to check the correctness:  
(`sample_text` is an arbitrary null-terminated string less than 100 Bytes.)  

```
# echo 'sample_text' > /dev/mychrdev
# cat /dev/mychrdev
sample_text
#
```


## Usage

Note that this module is tested in **CentOS 6**, **x86_64** with kernel version **2.6.39**.  

1. Compile the source code
    ```
    # make
    ```
2. Monitor system logs
    ```
    # tail -f /var/log/messages
    ```
3. Insert the module and check major number & minor number 
    ```
    # insmod ./mychrdev.ko
    ```
4. Create a character device (assume major number is 250 and minor number is 0)
    ```
    # mknod /dev/mychrdev c 250 0
    ```
5. Test the result
    ```
    # echo 'sample_text' > /dev/mychrdev
    # cat /dev/mychrdev
    ```



