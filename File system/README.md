# Linux kernel assignment: File system


## Goal

Implement a file system as described below:
1. The root structure of the file system should be:
    ```
    /--+ input (dir)
       |   |
       |   +-- a (file)
       |   +-- b (file)
       |
       + output (dir)
           |
           +-- add (file)
           +-- sub (file)
    ```

2. The values of `a` and `b` can be set by
    ```
    # echo number1 > /input/a
    # echo number2 > /input/b
    ```
    where `number1` and `number2` are numbers between 0 and 255  

3. We can get the values of a+b and a-b by
    ```
    # cat /output/add
    ```
    and 
    ```
    # cat /output/sub
    ```
    respectively


## Usage

Note that this module is tested in **CentOS 6**, **x86_64** with kernel version **2.6.39**.  

1. Compile the source code
    ```
    # make
    ```
2. Insert the module
    ```
    # insmod ./myfs.ko
    ```
3. Create a directory
    ```
    # mkdir fs
    ```
4. Mount `/dev/loop0` at `fs`
    ```
    # mount -t myfs /dev/loop0 fs
    ```
5. Test the result
    ```
    # cd ./fs
    # echo number1 > input/a
    # echo number2 > input/b
    # cat output/add
    # cat output/sub
    ```

