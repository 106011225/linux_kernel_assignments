#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/pagemap.h>      /* PAGE_CACHE_SIZE */
#include <linux/fs.h>           /* This is where libfs stuff is declared */
#include <asm/atomic.h>
#include <asm/uaccess.h>        /* copy_to_user */

#include <linux/slab.h>
 
 
MODULE_LICENSE("GPL");
 
#define MYFS_MAGIC 0x20220404
 
 
static struct inode *myfs_make_inode(struct super_block *sb, int mode)
{
        struct inode *ret = new_inode(sb);
 
        if (ret != NULL) {
                ret->i_mode = mode;
                ret->i_uid = ret->i_gid = 0; // set to 'root'
                ret->i_blocks = 0;
                ret->i_atime = ret->i_mtime = ret->i_ctime = CURRENT_TIME;
        }
        return ret;
}
 
static int myfs_open(struct inode *inode, struct file *filp)
{
        filp->private_data = inode->i_private;
        return 0;
}
 
#define TMPSIZE 20
 
static ssize_t myfs_read_file(struct file *filp, char *buf,
                size_t count, loff_t *offset)
{
    unsigned char* d_iname = filp->f_path.dentry->d_iname;
    int v, len;
    char tmp[TMPSIZE];

    if (!strcmp(d_iname, "a") || !strcmp(d_iname, "b")) { // var a or var b
        atomic_t *counter = (atomic_t *) filp->private_data;
        v = atomic_read(counter);
    }
    else if (!strcmp(d_iname, "add")){
        atomic_t** ptr_arr = (atomic_t**) filp->private_data;
        int a, b;
        a = atomic_read(ptr_arr[0]);
        b = atomic_read(ptr_arr[1]);
        v = a + b;
    }
    else if (!strcmp(d_iname, "sub")){
        atomic_t** ptr_arr = (atomic_t**) filp->private_data;
        int a, b;
        a = atomic_read(ptr_arr[0]);
        b = atomic_read(ptr_arr[1]);
        v = a - b;
    }
 
        /* if (*offset > 0) */
        /*         v -= 1;  [> the value returned when offset was zero <] */
        /* else */
        /*         atomic_inc(counter); */

        len = snprintf(tmp, TMPSIZE, "%d\n", v);
        if (*offset > len) // read done
                return 0;
        if (count > len - *offset)
                count = len - *offset;
 
        if (copy_to_user(buf, tmp + *offset, count)) // copy to user buf
                return -EFAULT; // segmentation fault
        *offset += count;
        return count;
}
 
static ssize_t myfs_write_file(struct file *filp, const char *buf,
                size_t count, loff_t *offset)
{
    unsigned char* d_iname = filp->f_path.dentry->d_iname;
    if (strcmp(d_iname, "a") && strcmp(d_iname, "b")) { // not var a nor var b
        return -EINVAL; 
    }
        atomic_t *counter = (atomic_t *) filp->private_data;
        char tmp[TMPSIZE];
 
        if (*offset != 0)
                return -EINVAL;
        if (count >= TMPSIZE)
                return -EINVAL;

        memset(tmp, 0, TMPSIZE);
        if (copy_from_user(tmp, buf, count))
                return -EFAULT;
 
        atomic_set(counter, simple_strtol(tmp, NULL, 10)); // setting 'counter', using str2long
        return count;
}
 
 
static struct file_operations myfs_file_ops = {
        .open   = myfs_open,
        .read   = myfs_read_file,
        .write  = myfs_write_file,
};
 
 
static struct dentry *myfs_create_file (struct super_block *sb,
                struct dentry *dir, const char *name,
                void *mydata) {

        struct dentry *dentry;
        struct inode *inode;
        struct qstr qname;
 
        qname.name = name;
        qname.len = strlen (name);
        qname.hash = full_name_hash(name, qname.len);
 
        dentry = d_alloc(dir, &qname);
        if (! dentry)
                goto out;
        inode = myfs_make_inode(sb, S_IFREG | 0644); // rwx...
        if (! inode)
                goto out_dput;

        inode->i_fop = &myfs_file_ops;
        inode->i_private = mydata;
 
        d_add(dentry, inode);
        return dentry;
 
  out_dput:
        dput(dentry);
  out:
        return 0;
}
 
 
static struct dentry *myfs_create_dir (struct super_block *sb,
                struct dentry *parent, const char *name)
{
        struct dentry *dentry;
        struct inode *inode;
        struct qstr qname;
 
        qname.name = name;
        qname.len = strlen (name);
        qname.hash = full_name_hash(name, qname.len);
        dentry = d_alloc(parent, &qname);
        if (! dentry)
                goto out;
 
        inode = myfs_make_inode(sb, S_IFDIR | 0644);
        if (! inode)
                goto out_dput;

        inode->i_op = &simple_dir_inode_operations;
        inode->i_fop = &simple_dir_operations;
 
        d_add(dentry, inode);
        return dentry;
 
  out_dput:
        dput(dentry);
  out:
        return 0;
}
 
 
/* static atomic_t counter, subcounter; */
static atomic_t *a_ptr, *b_ptr;
static atomic_t* ptr_arr[2];
 
static void myfs_create_files (struct super_block *sb, struct dentry *root)
{
        /* struct dentry *subdir; */
 
        /* atomic_set(&counter, 0); */
        /* myfs_create_file(sb, root, "counter", &counter); */
 
        /* atomic_set(&subcounter, 0); */
        /* subdir = myfs_create_dir(sb, root, "subdir"); */
        /* if (subdir) */
        /*         myfs_create_file(sb, subdir, "subcounter", &subcounter); */

    struct dentry *dir_input, *dir_output;
    a_ptr = (atomic_t*)kzalloc(sizeof(atomic_t), GFP_KERNEL); // malloc & set to 0
    b_ptr = (atomic_t*)kzalloc(sizeof(atomic_t), GFP_KERNEL);
    ptr_arr[0] = a_ptr;
    ptr_arr[1] = b_ptr;
    dir_input = myfs_create_dir(sb, root, "input");
    dir_output = myfs_create_dir(sb, root, "output");

    if (dir_input) {
        myfs_create_file(sb, dir_input, "a", (void*)a_ptr);
        myfs_create_file(sb, dir_input, "b", (void*)b_ptr);
    }
    if (dir_output) {
        myfs_create_file(sb, dir_output, "add", (void*)ptr_arr);
        myfs_create_file(sb, dir_output, "sub", (void*)ptr_arr);
    }
    

}
 
 
 
static struct super_operations myfs_s_ops = {
        .statfs         = simple_statfs,
        .drop_inode     = generic_delete_inode,
};
 
static int myfs_fill_super (struct super_block *sb, void *data, int silent)
{
        struct inode *root;
        struct dentry *root_dentry;
 
        sb->s_blocksize = PAGE_CACHE_SIZE;
        sb->s_blocksize_bits = PAGE_CACHE_SHIFT;
        sb->s_magic = MYFS_MAGIC;
        sb->s_op = &myfs_s_ops;
 
        // root inode
        root = myfs_make_inode (sb, S_IFDIR | 0755);
        if (! root)
                goto out;
        root->i_op = &simple_dir_inode_operations;
        root->i_fop = &simple_dir_operations;
 
        // root dentry
        root_dentry = d_alloc_root(root);
        if (! root_dentry)
                goto out_iput;
        sb->s_root = root_dentry;
 
        // root file
        myfs_create_files (sb, root_dentry);
        return 0;
 
  out_iput:
        iput(root);
  out:
        return -ENOMEM;
}
 
static struct dentry *myfs_get_super(struct file_system_type *fst,
                int flags, const char *devname, void *data)
{
        return mount_bdev(fst, flags, devname, data, myfs_fill_super);
}
 
static struct file_system_type myfs_type = {
        .owner          = THIS_MODULE,
        .name           = "myfs",
        .mount          = myfs_get_super,
        .kill_sb        = kill_litter_super,
};
 
static int __init myfs_init(void)
{
        return register_filesystem(&myfs_type);
}
 
static void __exit myfs_exit(void)
{
        unregister_filesystem(&myfs_type);
        kfree((void*)a_ptr);
        kfree((void*)b_ptr);
}
 
module_init(myfs_init);
module_exit(myfs_exit);
