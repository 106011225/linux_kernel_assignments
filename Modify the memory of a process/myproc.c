#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/printk.h>
#include <linux/ptrace.h>
#include <linux/mm.h>
#include <linux/mm_types.h>
#include <linux/page-flags.h>
#include <linux/pagemap.h>
#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/highmem.h>

struct task_struct *task;

#define MAX_PROC_SIZE 100
static char proc_data[MAX_PROC_SIZE];
char *endp;
int interested_pid=0;

static struct proc_dir_entry *proc_write_entry;

int read_proc(char *buf,char **start,off_t offset,int count,int *eof,void *data ) {
    printk(KERN_INFO "Inside read_proc, pid=%d", interested_pid);
    int total_len=0;
    for_each_process( task ){
        if(task->pid == interested_pid){

            struct mm_struct* ps_addr_space = task->mm; 

            int len;
            len = sprintf(buf,"\n%d %d OK\n",task->pid, ps_addr_space->map_count);
            total_len += len;

            struct page* user_pages;
            int numPinnedPages;
            int* myaddr;
            numPinnedPages = get_user_pages(task, ps_addr_space, ps_addr_space->start_stack, 1, 1, 1, &user_pages, NULL);
            if (numPinnedPages){
                myaddr = kmap(user_pages);
                printk(KERN_INFO "myaddr=%p", myaddr);

                int intBuf[1024];
                memcpy(intBuf, myaddr, 1024*sizeof(int));
                int i;
                for (i = 0; i < 1024-3; i++){
                    if (intBuf[i]==5 && intBuf[i+1]==6 && intBuf[i+2]==7 && intBuf[i+3]==8){
                        printk(KERN_INFO "intBuf[%d] = %d ", i, intBuf[i]);
                        printk(KERN_INFO "intBuf[%d] = %d ", i+1, intBuf[i+1]);
                        printk(KERN_INFO "intBuf[%d] = %d ", i+2, intBuf[i+2]);
                        printk(KERN_INFO "intBuf[%d] = %d ", i+3, intBuf[i+3]);
                        break;
                    }
                }
                int overwrite_val = 20220627;
                memcpy(myaddr+i, &overwrite_val, sizeof(int));
                kunmap(user_pages);
            }

        }
    }

    return total_len;
}

int write_proc(struct file *file,const char *buf,int count,void *data ) {

    printk(KERN_INFO "Inside write_proc");
    if(count > MAX_PROC_SIZE)
        count = MAX_PROC_SIZE;
    if(copy_from_user(proc_data, buf, count))
        return -EFAULT;
    interested_pid = simple_strtol(proc_data,&endp,10);
    return count;
}

static int proc_init(void) {
    proc_write_entry = create_proc_entry("myproc",0666,(void *)NULL);
    if(!proc_write_entry) {
        printk(KERN_INFO "Error creating proc entry");
        /* return; */
        return -ENOMEM;
    }
    proc_write_entry->read_proc = (void *)read_proc ;
    proc_write_entry->write_proc = (void *)write_proc;
    printk(KERN_INFO "proc initialized");
    return 0;
}

static void proc_cleanup(void) {
    printk(KERN_INFO " Inside cleanup_module\n");
    remove_proc_entry("myproc",NULL);
}

MODULE_LICENSE("GPL");
module_init(proc_init);
module_exit(proc_cleanup);
