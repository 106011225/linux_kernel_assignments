# Linux kernel assignment: Modify the memory of a process


## Goal

Given the pid and the source code of a process as below, implement a kernel module to modify the value of variable `a` so that the process prints out a number other than `5`.
```c
// test.c
#include <stdio.h>
#include <unistd.h>
int main() {
  int a=5;
  int b=6;
  int c=7;
  int d=8;
  printf("%d\n",getpid());
  while(1){
    printf("%d\n",a);
    sleep(1);
  }
  return 0;
}
```


## Usage 

Note that this module is tested in **CentOS 6**, **x86_64** with kernel version **2.6.39**.  

1. Compile and execute `test.c`
    ```
    $ gcc test.c -o test
    $ ./test
    ```
2. Compile the source code of the module 
    ```
    # make
    ```
3. Insert the module
    ```
    # insmod ./myproc.ko
    ```
4. Test the result (`<test_pid>` is the pid of process `test`)
    ```
    # echo <test_pid> > /proc/myproc
    # head /proc/myproc
    ```
    The value of `a` should be 20220627 now.
