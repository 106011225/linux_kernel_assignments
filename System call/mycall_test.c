#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#define __NR_mycall 307
#include <string.h>
#include <stdlib.h>
int main(int argc,char **argv)
{
    char str2[]="0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 ";
    char *str=malloc(20);
    strcpy(str, "123456");
    long int ret;
    if (argc>1 && argv[1][0]=='0'){
       ret = syscall(__NR_mycall, str);
       printf("System call sys_mycall returned %ld\n", ret);
    }
    if (argc>1 && argv[1][0]=='1'){
        ret = syscall(__NR_mycall, str, 6); // 6 indicates len(str)
        printf("System call sys_mycall returned %ld\n", ret); // should be 0, and the prof. will check the printk output
    }

    if (argc>1 && argv[1][0]=='2'){
       ret = syscall(__NR_mycall, str2);
       printf("System call sys_mycall returned %ld\n", ret);
    }
    if (argc>1 && argv[1][0]=='3'){
        ret = syscall(__NR_mycall, NULL, 0);
        printf("System call sys_mycall returned %ld\n", ret);
    }

    if (argc>1 && argv[1][0]=='4'){
       ret = syscall(__NR_mycall, "123");
       printf("System call sys_mycall returned %ld\n", ret);
    }
    if (argc>1 && argv[1][0]=='5'){
        ret = syscall(__NR_mycall, "123", 3);
        printf("System call sys_mycall returned %ld\n", ret);
    }

    free(str);
    return 0;

}
