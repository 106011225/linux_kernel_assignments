#include <linux/kernel.h>
#include <linux/string.h>
#include <asm-generic/errno-base.h>
asmlinkage long sys_mycall(char *str_user){

    if (str_user == NULL){
        printk(KERN_INFO "invalid address.\n");
        return -EFAULT;
    }
    else {
        char str_kernel[101] = {0}; 
        strncpy(str_kernel, str_user, 100);
        str_kernel[100] = '\0';

        printk(KERN_INFO "%s\n", str_kernel);
    } 

    return 0;
}
