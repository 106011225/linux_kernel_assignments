# Linux kernel assignment: System call


## Goal

Implement a system call `sys_mycall` as described below:
1. A string pointer is passed to `sys_mycall`.
2. `sys_mycall` prints out the string pointed to by the pointer with `printk()`.
3. The length of the string is less than 100 characters.


## Usage

Note that this module is tested in **CentOS 6**, **x86_64** with kernel version **2.6.39**.  

1. Copy [mycall/](./mycall) into `/usr/src/kernels/linux-2.6.39/`
2. Modify `/usr/src/kernels/linux-2.6.39/Makefile`
    - Append "`mycall/`" to the line "`core-y += kernel/ mm/ fs/ ipc/ security/ crypto/ block/`"
        ```Makefile
        core-y += kernel/ mm/ fs/ ipc/ security/ crypto/ block/ mycall/
        ```
    - Alternatively, replace `/usr/src/kernels/linux-2.6.39/Makefile` with [Makefile](./Makefile), which is in the current directory of this repo
3. Modify `/usr/src/kernels/linux-2.6.39/arch/x86/include/asm/unistd_64.h`
    - Add the new system call
        ```c
        #define __NR_mycall                             307 
        __SYSCALL(__NR_mycall, sys_mycall) 
        ```
    - Alternatively, replace `/usr/src/kernels/linux-2.6.39/arch/x86/include/asm/unistd_64.h` with [unistd_64.h](./unistd_64.h), which is in the current directory of this repo
4. Modify `/usr/src/kernels/linux-2.6.39/include/linux/syscalls.h`
    - Add the new system call
        ```c
        asmlinkage long sys_mycall(char* str_user);
        ```
    - Alternatively, replace `/usr/src/kernels/linux-2.6.39/include/linux/syscalls.h` with [syscalls.h](./syscalls.h), which is in the current directory of this repo
5. Recompile the kernel
    ```
    $ cd /usr/src/kernels/linux-2.6.39
    # make menuconfig
    # make -j4
    # make modules_install
    # make install
    # reboot
    ```
6. Test the result
    - Monitor system logs
    ```
    # tail -f /var/log/messages
    ```
    - Compile and execute [mycall_test.c](./mycall_test.c), which is in the current directory of this repo
    ```
    $ gcc mycall_test.c -o mycall_test
    $ ./mycall_test
    ```

    
